
# SPRINT 2017-05-22

---

1. Demo: Space Certified
2. Demo: MODHEM indicators
3. Demo: Misc
4. Planning

---

# SPACE CERTIFIED

+++

## Context + Example

---

# MODHEM

### Pasture quality indicators

+++

## Vegetation indices but ...

+++

## Issues

1. Estimation of biomass level.
2. Soil background effects.
3. Insensitivity to nonphotosynthetic vegetation (NPV).

+++

## Indices

* NDVI / iNDVI
* SAVI (Soil Adjusted Vegetation Index)
* MSAVI (Modified Soil Adjusted Vegetation Index)
* SWVI: replacing RED by SWIR
* SNDVI [or NDSVI]: replacing NIR by SWIR 

---

# MISC

+++

* Global Peat Initative
* Visit EODC
* SL Architecture

---

# PLANNING

+++

* Service Line
* Deliverables Q2
* Projects, Acquisition & Marketing

+++

## Service Line

* MODIS NDVI 1.0
* Plan Landsat 2 DPROF
* Plan S1 2 EODC / DPROF

+++

## Deliverables Q2

* Story outlines
* Presentation outlines
* Web outlines
* Streamline with project et al deliverables

+++

## Projects et al

* FCC Paraguay (Q2D)
* Myanmar update maps (Q2D)
* Myanmar update report (Q2D)
* GoldenPaddy G4AW proposal
* ?

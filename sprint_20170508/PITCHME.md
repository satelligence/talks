
# SPRINT 2017-05-17

---

1. Demo: Roles
2. Demo: Business Model
3. Demo: Elevator Pitch
4. Demo: Algorithms / AI
5. Planning

---

# ROLES

+++

## > Q1 2017

* Good team
* Positive culture
* Interesting and challenging work

## > Q2 2017

* Confirmed and explicit roles
* Clear goals and framing

+++

## General

* Everybody is RS expert
* Everybody knows about radar
* We have no juniors
* We are a network organisation

+++

## Roles

* Michel: analyst, post-process structuring, forest & agri
* Loes: liaison (West) Africa, agri & surface water
* Nanne: liaison SE Asia, acquisition (?)
* Ernst: coordination, data intimacy, processing
* Berto + Vincent: architecture, algorithms, processing
* Rob: radar, liaison knowledge partners

---

# BUSINESS MODEL

### Customer driven
### Trust based

---

# ELEVATOR PITCH

+++

FOR _plantation businesses_, WHO NEED TO  _control deforestation_, the _Forest Change Bulletin_ IS A _monitoring and evaluation tool_ THAT _enables them to comply to sustainable production regulations_. UNLIKE _Global Forest Watch_, THE PRODUCT _is timely and accurate_.

+++

FOR _agri information businesses (like call centers)_, WHO NEED TO  _locate water resources for pastoralists_, the _water reservoir map_ IS A _planning tool_ THAT _enables them to prevent income loss and conflict_. UNLIKE _mouth to mouth information_, THE PRODUCT _is timely and reliable_.

---

# ALGORITHMS / AI

+++

## Yield prediction

---

# PLANNING

+++

* Service Line
* Algorithms / AI
* Deliverables Q2
* Projects, Acquisition & Marketing
* Ideas

+++

## Service Line

* Visualise roadmap
* DPROF, MODIS: 1.0
* S1 2 EODC: backlog
* DPROF, Landsat: backlog
* DPROF, S2: backlog
* Dissemination req.: user job matrix

+++

## Algorithms / AI

* Structure, plan, backlog

+++

## Deliverables Q2

* Story outlines
* Presentation outlines
* Web outlines
* Streamline with project et al deliverables

+++

## Projects et al

* FCC Paraguay (Q2D)
* FCC Global Peat Initiative (Q2D)
* Process Philippines
* Myanmar update maps (Q2D)
* Myanmar update report (Q2D)
* ?

+++

## Ideas

* Quarterly essay / bulletin

  * GPI
  * Elephants & microscopes: MODIS
  * RS & AI for Pastoralists

* Swim / Pitch sessions
* BBQ
* Employee handbook
* Marketing & Acquisition plans
* Build Utrecht-SAT?

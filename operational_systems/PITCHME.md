
# OPERATIONAL PRODUCTION SYSTEMS

---

1. Operational

2. Production

3. Systems

---?video=assets/working.mp4

# OPERATIONAL

+++

A system that is used to process the day-to-day transactions of an organisation.

+++

1. Stable

2. Scalable

3. Repeatable

4. Cost efficient


+++?image=assets/production/dam.jpg

## Stable

part of value chain, downstream users, predictable, dependable

+++?image=assets/production/coffee.jpg

## Scalable

predictable, consultancy or products, cost

+++?image=assets/production/repeat.jpg

## Repeatable

bus factor, attribute of system not of person

+++?image=assets/production/efficient.jpg

## Cost efficient

marginal cost to 0

---?video=assets/production/growth.mp4

# PRODUCTION

+++

# Development funnel

+++?image=assets/production/Innovation-Pipeline.jpg

+++

## Idea! 2 product

+++?image=assets/production/Innovation-Funnel.jpg

+++

## Operational product

* Minimise variation:
  * Stable
  * Predictable
  * Low marginal cost

+++

## Idea development

* Increase variation


+++

## cost structure of development

+++?image=assets/production/dev_cost.png

+++

# Production

## Minimise marginal cost

+++

# Development

## Minimise cost of _change_

+++

## Examples minimising cost of change

1. Collaborative proposal writing
2. Software development

+++

## Example 1: collaborative proposal writing

+++?image=assets/production/collaborative_editing.png

+++?image=assets/production/messages.png

High coordination and transaction cost

+++

## Google Docs

* Real time collaborative editing (no emails)
* Versioning
* (Automated spell checking)
* Low transaction & coordination costs

+++

## Example 2: software dev

+++

### Contain variance: separate environments

+++

## DISP

1. Development

2. Integration

3. Staging

4. Production

+++

## Reduce Transaction cost

+++

* Packaging & release (versionig)
* Automated provisioning
* 1 step deploy
* Automated testing

+++?image=assets/production/deployment_environments.png

+++

* Continuous integration
* Continuous deployment
* Containerisation (Docker)

+++?image=assets/production/continuous_deployment.jpg

+++

## Reduce coordination cost

+++

## Development methods

* Lean
* Kanban
* Scrum
* Sigma six
* Design thinking
* ...

+++

## Problems solved

1. Complexity & Chaos
2. Learning and adaptation

+++

## Contain complexity & chaos

* Patterns
* Containers

+++

## Attributes of self learning, adaptive systems

* Feedback loops
* Resilient
* Agile
* Low cost of change

---?video=assets/production/dogs.mp4

# SYSTEMS

+++

* Multiple components
* Information exchange betweeen components
* Closed or open

+++?image=assets/production/steam_engine.png

+++

## Components & environments

* Persons in team
* Teams in organisation
* Organisations in value chain

+++

## Scaling

* Engineering
* Customers
* Data science
* Partners

+++

## Cases

+++?image=assets/production/andon.png

+++?image=assets/production/donald.jpg

+++?image=assets/production/7s.jpg

+++?image=assets/production/capsize.jpg

+++?image=assets/production/google_sre.png

## Embrace risk

+++

## RSNL
